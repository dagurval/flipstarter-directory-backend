from functools import reduce

class FlipstarterCommandMixin:


    def nothing(self, ret):
        """
        Receives a tuple with the return of the command
        tuple (success, failed, got_exception)
        """
        if reduce(lambda x, y: x + y, ret) == 0:
            self.stdout.write(self.style.SUCCESS('Nothing to do.'))
            return True
        return False

    def print_success_message(self, message, count):
        if count == 0:
            return
        plural = 's' if count > 1 else ''
        self.stdout.write(self.style.SUCCESS(
            message.format(count=count, plural=plural)
        ))

    def print_error_message(self, message, count):
        if count == 0:
            return
        plural = 's' if count > 1 else ''
        self.stderr.write(self.style.ERROR(
            message.format(count=count, plural=plural)
        ))