import bleach
from socket import timeout
from urllib.error import URLError
from django.core.exceptions import ValidationError

from rest_framework import fields, serializers

from . import models
from .bitcoin import satoshi_to_bch


class FlipstarterArchiveSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.FlipstarterArchive
        fields = ('url', )


class FlipstarterScreenshotSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.FlipstarterScreenshot
        fields = ('image', 'taken_at', 'taken_status')


class FlipstarterAnnouncementSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.FlipstarterAnnouncement
        fields = ('url', )


class FlipstarterCategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = models.FlipstarterCategory
        fields = ('name', )
        # we need to remove the validator for uniqueness since
        # since we use this nested
        # https://stackoverflow.com/questions/38438167/unique-validation-on-nested-serializer-on-django-rest-framework
        extra_kwargs = {
            'name': {'validators': []},
        }


class FlipstarterSerializer(serializers.ModelSerializer):
    categories = FlipstarterCategorySerializer(many=True)
    announcements = FlipstarterAnnouncementSerializer(many=True)
    archives = FlipstarterArchiveSerializer(many=True)
    screenshots = FlipstarterScreenshotSerializer(read_only=True, many=True)
    # limits the number of archives and announcements
    # that can be provided
    # does not apply to categories, that are limited only
    # existing ones
    MAX_ITEMS = 3

    class Meta:
        model = models.Flipstarter
        fields = (
            'amount', 'title', 'description', 'url',
            'status', 'funded_tx', 'categories', 'announcements',
            'archives', 'screenshots'
        )

    def update(self, instance, validated_data):
        """
        Must be implemented if you decide to use update
        """
        pass

    def create(self, validated_data):
        """
        override create so we don't create new categories
        but instead find any existing ones. If none are found
        then we don't add any.
        """
        # clean all fields that are strings from any html
        categories = list(map(lambda c: c.get('name', '').lower(),  validated_data.pop('categories')))

        announcements = validated_data.pop('announcements')
        archives = validated_data.pop('archives')

        # clean every text field
        validated_data['title'] = bleach.clean(validated_data.get('title', ''), strip=True)
        validated_data['description'] = bleach.clean(validated_data.get('description', ''), strip=True)
        # all other fields have their own validators from the model and specific model field

        flipstarter = models.Flipstarter.objects.create(
            **validated_data
        )
        categories_to_add_to = models.FlipstarterCategory.objects.filter(
            iname__in=categories
        )
        flipstarter.categories.add(*categories_to_add_to)

        for announcement_kwargs in announcements:
            models.FlipstarterAnnouncement.objects.create(flipstarter=flipstarter, **announcement_kwargs)

        for archive_kwargs in archives:
            models.FlipstarterArchive.objects.create(flipstarter=flipstarter, **archive_kwargs)

        return flipstarter

    def to_internal_value(self, data):
        """
        enforce limits on the number of announcements and archives
        """
        ret = super().to_internal_value(data)
        if len(ret.get('announcements', [])) > self.MAX_ITEMS:
            raise ValidationError(f'Max {self.MAX_ITEMS} announcements.')
        if len(ret.get('archives', [])) > self.MAX_ITEMS:
            raise ValidationError(f'Max {self.MAX_ITEMS} archives.')
        return ret


class FlipstarterJSONFormatSerializer(serializers.Serializer):
    """
    Returns the flipstarter data with the fields and format of the JSON file.
    Can be used for backwards compatibility.
    """
    def create(self, validated_data):
        """
        Only for backward compatibility output/exporting
        """
        pass

    def update(self, instance, validated_data):
        """
        Only for backward compatibility output/exporting
        """
        pass

    def to_representation(self, instance):
        """
        Returns the representation as JSON file that
        was present in the frontend
        """
        ret = {}
        ret['amount'] = satoshi_to_bch(instance.amount)
        ret['title'] = instance.title
        ret['description'] = instance.description
        ret['category'] = list(map(lambda c: c.name, instance.categories.all()))
        ret['status'] = instance.status
        ret['tx'] = instance.funded_tx
        ret['url'] = instance.url
        ret['announcement'] = list(map(lambda a: a.url, instance.announcements.all()))
        # archives in the old format are composed of
        # FlipstarterArchive and FlipstarterScreenshot
        # we use the image field from serializers so it can attach the url
        # we could also just copy this behaviour
        def get_screenshot_url(image):
            image_field = fields.ImageField()
            image_field.bind('screenshot', self)
            return image_field.to_representation(image)

        archives = list(map(lambda a: a.url, instance.archives.all()))
        screenshots = list(map(lambda s: get_screenshot_url(s.image), instance.screenshots.all()))
        ret['archive'] = archives + screenshots
        return ret


class FlipstarterCampaignURLSerializer(serializers.Serializer):
    url = serializers.URLField(required=True, write_only=True)
    # read-only data

    def fetch_data_from_url(self):
        """
        Fetches the data using the url provided to the serializer
        If no data can be provided then it returns an error
        """
        url = self.validated_data.get('url')
        fs_kwargs = {}
        try:
            # we don't actually create the object
            # but we store it for later
            # we send this info back so the user
            # can correct ammend some of it
            fs_kwargs = models.Flipstarter.objects.flipstarter_kwargs_from_url(url)
            self.flipstarter = models.Flipstarter(
                id=-1, # dummy id for serialization to work
                **fs_kwargs
            )
            self.error = None
        except (URLError, timeout):
            # since we can't recover anything we return an keep an Flipstarter
            self.error = 'Unable to retrieve flipstarter from url.'

    def to_representation(self, validated_data):
        """
        This will be called with validated data because our
        serializer doesn't set an instance
        @validated_data we don't need it here
        """
        if not hasattr(self, 'error'):
            return {
                'error': 'Failed.'
            }
        elif self.error is None and self.flipstarter is not None:
            return {
                'campaign': FlipstarterSerializer().to_representation(self.flipstarter)
            }
        else:
            return {
                'error': self.error
            }
