import logging
import json
import re
from concurrent import futures
from datetime import datetime, timedelta
from functools import reduce
from pytz import UTC
from socket import timeout
from urllib.error import URLError
from urllib.parse import urlparse
from urllib.request import Request as UrllibRequest, urlopen

from django.conf import settings
from django.core.files.images import ImageFile
from django.db import models
from django.db.models import F, Subquery
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from .exceptions import FlipstarterUnavailableException
from .flipstarter import create_flipstarter, take_flipstarter_screenshot
from .utils import is_postgresql_or_mariadb, AJAX_TIMEOUT
from .validators import RegexOrEmptyValidator


logger = logging.getLogger(__name__)


# Create your models here.
class FlipstarterCategoryManager(models.Manager):

    def create(self, *args, **kwargs):
        kwargs['iname'] = kwargs.get('name').lower()
        return super().create(*args, **kwargs)

class FlipstarterCategory(models.Model):
    """
    Category has a name which must be unique
    and a iname which must also be unique.
    iname is the same but case insentive to help
    us with finding the category and also guaranteeing no duplicates
    such as Software and software.
    """
    # category name
    name = models.CharField(
        max_length=100,
        unique=True
    )
    iname = models.CharField(
        max_length=100,
        unique=True
    )

    objects = FlipstarterCategoryManager()

    def __str__(self) -> str:
        return f'{self.name}'


class FlipstarterManager(models.Manager):

    def get_flipstarter_description(self, parsed_url):
        """
        Return the campaign abstract/description
        @parsed_url url that has been ran through urlparse
        """
        campaign_id = int(parsed_url.fragment or 1)
        response = urlopen(
            UrllibRequest(
                f'{parsed_url.scheme}://{parsed_url.netloc}/static/campaigns/{campaign_id}/en/abstract.md',
                headers={'User-Agent': 'flipstarterdirectory'}
            ),
            timeout=AJAX_TIMEOUT
        )
        return response

    def get_flipstarter_campaign_data(self, parsed_url):
        """
        returns campaign data
        @parsed_url url that has been ran through urlparse
        """
        campaign_id = int(parsed_url.fragment or 1)
        response = urlopen(
            UrllibRequest(
                f'{parsed_url.scheme}://{parsed_url.netloc}/campaign/{campaign_id}',
                headers={'User-Agent': 'flipstarterdirectory'}
            ),
            timeout=AJAX_TIMEOUT
        )

        return json.loads(response.read())

    def extract_data_from_campaign_url(self, parsed_url):
        json_data = Flipstarter.objects.get_flipstarter_campaign_data(parsed_url)

        campaign_data = json_data.get('campaign')

        expires = Flipstarter.objects.timestampToDatetime(campaign_data, 'expires')

        funded_tx = campaign_data.get('fullfillment_transaction', None) or ''
        status = Flipstarter.objects.calculate_status(expires, funded_tx)
        kwargs = {
            'last_updated': timezone.now(),
            'status': status,
            'funded_tx': funded_tx,
            'expires': expires,
            'starts': Flipstarter.objects.timestampToDatetime(campaign_data, 'starts')
        }

        return kwargs

    def import_from_json(self, json_data):
        """
        receives a dict, with the json data
        this means the caller needs to load the json data
        json.load or json.loads for example

        @json_data the json data as a python dict
        return Tuple with successfully import and total that
               we tried to import. (3, 5) managed to import 3
               tried to import 5.
        """
        # keep a dict of categories to avoid
        # loading over and over from the db
        # we do this first so we can then send this
        # to the threads and not worry about race conditions
        # since we only read and have already create all\
        # the categories needed
        all_categories = set()
        for flipstarter_data in json_data:
            for category in flipstarter_data.get('category', []):
                if category != '':
                    all_categories.add(category)
        # fetch existing categories and create dict
        existing_categories = {c.name: c for c in FlipstarterCategory.objects.filter(name__in=all_categories)}
        # find all the categories that don't exist
        non_existing_categories = list(map(
            (lambda c: FlipstarterCategory(name=c, iname=c.lower())),
            filter((lambda c: c not in existing_categories), all_categories)
        ))

        # see bulk create note in regards to primary key auto field
        # https://docs.djangoproject.com/en/3.1/ref/models/querysets/#django.db.models.query.QuerySet.bulk_create
        if not is_postgresql_or_mariadb():
            # if we are not getting the id we might as well just manually save each
            # new entry and get the id, instead of refreshing each one after
            for category in non_existing_categories:
                category.save()
        else:
            non_existing_categories = FlipstarterCategory.objects.bulk_create(
                non_existing_categories
            )
        # now join the existing and the new ones all in categories
        categories = existing_categories
        for category in non_existing_categories:
            categories[category.name] = category

        success_count = 0
        with futures.ThreadPoolExecutor(max_workers=8) as executor:
            future_to_flipstarter = {
                executor.submit(create_flipstarter, flipstarter_data, categories): flipstarter_data
                for flipstarter_data in json_data
            }
            for future in futures.as_completed(future_to_flipstarter):
                try:
                    success = future.result()
                    if success:
                        success_count += 1
                except Exception as exc:
                    pass

        return (success_count, len(json_data))

    def calculate_status(self, expires, funded_tx):
        """
        returns the status of the flipstarter
        absed on the expires datetime and if there
        a funded_tx
        @expires the date the flipstarter expires/ends
        @funded_tx if the flipstarter was successful then
                   the transaction that funded it
        return the status choice of models.Flipstarter.Status
        """
        status = Flipstarter.Status.RUNNING
        # can succeeded before expiring
        if funded_tx:
            status = Flipstarter.Status.SUCCESS
        elif timezone.now() > expires:
            status = Flipstarter.Status.EXPIRED

        return status

    def timestampToDatetime(self, data, key):
        """
        fetches the unix timestamp from data with key
        and returns the a datetime object
        @data a dict
        @key the dict key that maps to the unix timestamp
        return datetime the unix timestamp converted to a datetime object
        """
        if data.get(key, None) is not None:
            return UTC.localize(datetime.utcfromtimestamp(data.get(key)))
        return None

    def flipstarter_kwargs_from_url(self, url):
        parsed_url = urlparse(url)
        json_data = self.get_flipstarter_campaign_data(parsed_url)
        campaign_data = json_data.get('campaign')
        recipient_data = json_data.get('recipients')
        description = self.get_flipstarter_description(parsed_url).read().decode('utf-8')

        starts = self.timestampToDatetime(campaign_data, 'starts')
        expires = self.timestampToDatetime(campaign_data, 'expires')
        status = self.calculate_status(expires, campaign_data.get('fullfillment_transaction', None))

        return {
            'title': campaign_data.get('title'),
            'amount': reduce((lambda x, y: x + y.get('recipient_satoshis', 0)), recipient_data, 0),
            'description': description,
            'url': url,
            'funded_tx': campaign_data.get('fullfillment_transaction', None) or '',
            'approved': True,
            'starts': starts,
            'expires': expires,
            'status': status
        }

    def import_from_url(self, url):
        """
        uses the flipstarter url to fetch the data needed
        @url a valid url to a flipstarter
        return the newly created flipstarter
        """
        return self.create(
            **self.flipstarter_kwargs_from_url(url)
        )

    def refresh_all(self):
        """
        Refreshes all flipstarters
        Might take a while
        """
        flipstarters = self.filter(
            approved=True,
            status=Flipstarter.Status.RUNNING,
            url_retry__gt=0
        )

        refreshed_count = 0
        failed_count = 0
        exception_count = 0
        # refresh the status
        with futures.ThreadPoolExecutor(max_workers=8) as executor:
            future_to_flipstarter = {
                executor.submit(lambda f: f.refresh_flipstarter(), flipstarter): flipstarter
                for flipstarter in flipstarters
            }
            for future in futures.as_completed(future_to_flipstarter):
                try:
                    refreshed = future.result()
                    if refreshed:
                        refreshed_count += 1
                    else:
                        failed_count += 1
                except Exception as exc:
                    flipstarter = future_to_flipstarter[future]
                    logger.warning(f'{flipstarter.id} - {flipstarter.title} Failed to refresh {exc}')
                    exception_count += 1

        return (refreshed_count, failed_count, exception_count)

    def get_screenshots(self):
        """
        Gets/Creates screenshots for all the flipstarters.
        That don't have one.
        Even if other url are present as archives, if we didn't
        create it then we create one screenshot so we don't rely
        on other archives websites.
        """
        # all flipstarters that have a screenshot for
        # the current status they are in
        # our get_screenshot function already does this
        # but this here avoids us creating multiple threads
        # that will just exit
        done_flipstarters = self.filter(
            approved=True,
            screenshots__taken_status=F('status')
        ).values('id')
        # now use that as a subquery in the exclude
        # so we get all flipstarters which didn't have
        # a screenshot with their current status
        flipstarters_to_screenshot = self.exclude(
            id__in=Subquery(
                done_flipstarters
            )
        ).filter(approved=True)
        # now do the hanky panky and screenshot everything needed
        # because of the way the flipstarter frontend is built
        # we wait 10s to allow all ajax requests to finish
        # and the page to load fully. For that reason
        # the threads spend a lot of time sleeping
        # so we can be more generous with the number of threads
        # but we have to be careful with the amount of memory
        # if you have more memory to spare then you can increase this
        # we might consider putting this as a settings
        # the default is quite conservative as it can take 100MB+ per thread
        screenshot_count = 0
        failed_count = 0
        exception_count = 0
        # refresh the status
        with futures.ThreadPoolExecutor(
            max_workers=settings.FLIPSTARTER_DIRECTORY_SCREENSHOT_THREADS
        ) as executor:
            future_to_flipstarter = {
                executor.submit(lambda f: f.get_screenshot(), flipstarter): flipstarter
                for flipstarter in flipstarters_to_screenshot
            }
            for future in futures.as_completed(future_to_flipstarter):
                flipstarter = future_to_flipstarter[future]
                try:
                    got_screenshot = future.result()
                    if got_screenshot:
                        screenshot_count += 1
                    else:
                        failed_count += 1
                except Exception as exc:
                    logger.warning(f'{flipstarter.id} - {flipstarter.title} Failed to screenshot {exc}')
                    exception_count += 1

        return (screenshot_count, failed_count, exception_count)


URL_RETRIES = 3


class Flipstarter(models.Model):
    class Status(models.TextChoices):
        SUCCESS = 'success', _('Success')
        EXPIRED = 'expired', _('Expired')
        RUNNING = 'running', _('Running')
    # flipstart total raise ask in satoshis
    amount = models.BigIntegerField(verbose_name='Amount in Satoshis')
    # title of the flipstarter
    title = models.CharField(max_length=200)
    # description
    description = models.TextField()
    # url for the flipstarter
    url = models.URLField()
    # if the url starts failing we attempt URL_RETRIES times before
    # giving up forever
    url_retry = models.SmallIntegerField(default=URL_RETRIES)
    # status
    status = models.CharField(
        max_length=7,
        choices=Status.choices,
        default=Status.RUNNING
    )
    # transaction that funded the flipstarter
    funded_tx = models.CharField(
        blank=True,
        max_length=64,
        validators=[
            RegexOrEmptyValidator(regex=re.compile(r'^[1234567890abcdef]{64}$', flags=re.IGNORECASE))
        ]
    )
    categories = models.ManyToManyField(FlipstarterCategory)
    starts = models.DateTimeField(null=True, blank=True,)
    expires = models.DateTimeField(null=True, blank=True,)
    last_updated = models.DateTimeField(default=timezone.now)
    added = models.DateTimeField(default=timezone.now, editable=False)

    # whether this is valid and can be displayed in the directory
    approved = models.BooleanField(default=False)

    objects = FlipstarterManager()

    def __str__(self) -> str:
        return f'Id: {self.id} - {self.title}'

    def url_failed(self):
        """
        url retry
        """
        self.url_retry = models.F('url_retry') - 1
        self.save()

    def update(self, **kwargs):
        """
        Updates the fields that are present in kwargs
        Some fields such as amount and added are not editable
        return None
        """
        self.title = kwargs.get('title', self.title)
        self.description = kwargs.get('description', self.description)
        self.url = kwargs.get('url', self.url)
        self.url_retry = kwargs.get('url_retry', self.url_retry)
        self.status = kwargs.get('status', self.status)
        self.funded_tx = kwargs.get('funded_tx', self.funded_tx)
        self.starts = kwargs.get('starts', self.starts)
        self.expires = kwargs.get('expires', self.expires)
        self.last_updated = kwargs.get('last_updated', self.last_updated)
        self.save()

    def refresh_flipstarter(self, force=False):
        """
        refreshes the flipstarter using the url from the flipstarter
        this allows us to check if the flipstarter succeeded or failed
        as well as get the funding tx
        @force allows us to force the update even if not needed
        return None
        """
        if not force and self.url_retry <= 0:
            # url is considered dead
            # probably server was shutdown after the flipstarter
            return False
        if not force and (self.status == Flipstarter.Status.EXPIRED or
            self.status == Flipstarter.Status.SUCCESS):
            # nothing to do since it has already finished let's not update
            return False
        # if 1 hour has not passed since the last update do nothing
        if not force and not self.last_updated < timezone.now() - timedelta(hours=1):
            return False

        try:
            parsed_url = urlparse(self.url)
            kwargs = Flipstarter.objects.extract_data_from_campaign_url(parsed_url)
            kwargs['last_updated'] = timezone.now()
            kwargs['url_retry'] = URL_RETRIES
            # if the status changed then it needs an update.
            if self.status != kwargs['status'] or self.url_retry < URL_RETRIES:
                self.update(**kwargs)
            return
        except (URLError, timeout) as exc:
            self.url_failed()
            logger.warning(f'Url Failed for {self.title} - {self.url} - {exc}')
            return False

    def get_screenshot(self, force=False):
        """
        Updates the archive with an image for the current status
        if needed
        @force allows us to force the update even if not needed
        return None
        """
        if not force and self.url_retry <= 0:
            # url is considered dead
            # probably server was shutdown after the flipstarter
            return False

        if not force and self.screenshots.filter(taken_status=self.status).exists():
            # we already have an archive for this status
            return False

        now = timezone.now()
        try:
            print('c', settings.FLIPSTARTER_DIRECTORY_CHROME_REMOTE)
            image, file_name = take_flipstarter_screenshot(
                url=self.url,
                title=self.title,
                status=self.status,
                taken_at=now,
                chrome_remote=settings.FLIPSTARTER_DIRECTORY_CHROME_REMOTE
            )
        except FlipstarterUnavailableException as exc:
            logger.warning(f'Screenshot failed: {exc}')
            self.url_failed()
            return False

        self.screenshots.create(
            image=ImageFile(image, file_name),
            taken_at=now,
            taken_status=self.status
        )
        return True


class FlipstarterArchive(models.Model):
    """
    Flipstarter archives can be manually uploaded screenshots
    or urls to things such as archive.org or archive.is
    """
    flipstarter = models.ForeignKey(
        Flipstarter,
        on_delete=models.CASCADE,
        related_name='archives'
    )
    url = models.URLField(
        help_text='URL for an external source. If file is supplied then this is ignored.'
    )

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=['flipstarter', 'url'],
                name='unique_flipstarter_archive_url'
            )
        ]

    def __str__(self) -> str:
        return f'Id: {self.id} - {self.url} - {self.flipstarter.title}'


class FlipstarterScreenshot(models.Model):
    flipstarter = models.ForeignKey(
        Flipstarter,
        on_delete=models.CASCADE,
        related_name='screenshots'
    )
    image = models.ImageField(upload_to='screenshots/')
    taken_at = models.DateTimeField()
    # this represents an archive that we know was taken_at the taken_status
    # this way we know this represents the flipstarter on status SUCCESS
    # we aim to have an archive for every status
    taken_status = models.CharField(
        max_length=7,
        choices=Flipstarter.Status.choices,
    )

    def __str__(self) -> str:
        return f'Id: {self.id} - {self.image} - {self.flipstarter.title}'


class FlipstarterAnnouncement(models.Model):
    flipstarter = models.ForeignKey(
        Flipstarter,
        on_delete=models.CASCADE,
        related_name='announcements'
    )
    url = models.URLField()

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=['flipstarter', 'url'],
                name='unique_flipstarter_announcement_url'
            )
        ]

    def __str__(self) -> str:
        return f'Id: {self.id} - {self.url} - {self.flipstarter.title}'
