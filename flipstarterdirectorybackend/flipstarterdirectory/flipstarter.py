import os
import logging
from pytz import utc
from selenium import webdriver
from selenium.common.exceptions import WebDriverException
from selenium.webdriver.chrome.options import Options
from time import sleep

from django.utils.text import slugify

from . import models
from .bitcoin import bch_to_satoshi
from .exceptions import FlipstarterUnavailableException
from .utils import AJAX_TIMEOUT, WEBSITE_TIMEOUT


logger = logging.getLogger(__name__)


class TemporaryFile:
    """
    Temporary file on disk that gets deleted upon destruction
    of this class
    """
    def __init__(self, file_path):
        """
        opens the file in file_path
        """
        self.file_path = file_path
        self.file = open(file_path, 'rb')

    def __getattr__(self, name):
        """
        pass through and call the attributes of file
        """
        return getattr(self.file, name)

    def __del__(self):
        """
        close the file
        and then remove it
        """
        self.file.close()
        os.remove(self.file_path)


def make_kwargs(flipstarter_data):
    """
    helper function to extract data from the flipstarter_data dict
    attempts to extract data from the dict, then tries to extract
    from the url, to update some fields.
    @flipstarter_data the json data for a particular flipstarter
                      converted to a dict
    """
    kwargs = {
        'amount': bch_to_satoshi(flipstarter_data.get('amount', 0)),
        'title': flipstarter_data.get('title', ''),
        'description': flipstarter_data.get('description', ''),
        # some entries for tx have null
        'url': flipstarter_data.get('url', ''),
        'status': flipstarter_data.get('status', models.Flipstarter.Status.RUNNING),
        'funded_tx': flipstarter_data.get('tx', '') or '',
        # imported flipstarters are assumed to already be validated
        'approved': True
    }

    return kwargs

def create_flipstarter(flipstarter_data, categories):
    """
    Create a flipstarter from json data
    This is used to import flipstarter data from a JSON file
    @flipstarter_data the json data for a particular flipstarter
                      converted to a dict
    @categories a dict with the categories already loaded, to avoid loading or
                trying to create the same category twice
    """
    flipstarter = models.Flipstarter.objects.create(
        **make_kwargs(flipstarter_data)
    )

    # (create if needed) and add categories to the flipstarter
    categories_to_add = []
    for category in flipstarter_data.get('category', []):
        if category == '':
            continue
        if category not in categories:
            raise Exception(f'Category not in category cache {category}')
        categories_to_add.append(categories[category])
    if categories_to_add:
        flipstarter.categories.add(*categories_to_add)

    # helper function to add url based entries (archive, announcement)
    def create_url_entries(model, key):
        new_entries = []
        for url in flipstarter_data.get(key, []):
            new_entries.append(model(
                flipstarter=flipstarter,
                url=url,
            ))
        model.objects.bulk_create(new_entries)
    # add archive urls
    create_url_entries(models.FlipstarterArchive, 'archive')
    # add announcement urls
    create_url_entries(models.FlipstarterAnnouncement, 'announcement')
    return True

def take_flipstarter_screenshot(url, title, status, taken_at, chrome_remote=None):
    """
    Takes a screenshot of the flipstarter at url
    title, status and taken_at are used for screenshot name generation

    @url the url of the flipstarter
    @title the flipstarter title
    @status the current status we have on record for the flipstarter
            running, expired, success
    @taken_at the time this screenshot is being taken at
    @return url
    """
    print('c2', chrome_remote)
    if chrome_remote is None:
        logger.warning('No chrome remote set.')
        raise Exception('No chrome remote set.')

    title_formatted = slugify(title)
    taken_at_utc = taken_at.now(utc).isoformat()
    screenshot_name = f'{title_formatted}_{status}_{taken_at_utc}.png'

    screenshot_path = f'/tmp/{screenshot_name}'

    chrome_options = Options()
    chrome_options.add_argument("--disable-extensions")
    chrome_options.add_argument("--disable-gpu")
    chrome_options.add_argument("--headless")

    # we can't use a width based on scrollWidth
    # because the page is reactive and will adjust to
    # whatever width
    # 1920 gaves us a nice screenshot
    width = 1920

    driver = None
    try:
        driver = webdriver.Remote(
            command_executor=chrome_remote,
            options=chrome_options
        )
        driver.set_page_load_timeout(WEBSITE_TIMEOUT)
        driver.get(url)
        # https://stackoverflow.com/a/52572919/1321009
        # Ref: https://stackoverflow.com/a/52572919/
        original_size = driver.get_window_size()
        # we have to wait AJAX_TIMEOUT seconds because of the ajax requests to populate the page
        # AJAX_TIMEOUT is an arbritary number that seemed like good enough for most flipstarters
        sleep(AJAX_TIMEOUT)
        # required_width = driver.execute_script('return document.body.parentNode.scrollWidth')
        required_height = driver.execute_script('return document.body.parentNode.scrollHeight')
        driver.set_window_size(width, required_height)
        driver.find_element_by_tag_name('body').screenshot(screenshot_path)  # avoids scrollbar
    except Exception as exc:
        # if webdriver we assume the website loading failed
        # Webdriver throws a bunch of WebDriverException with the message
        # detailing the error
        if isinstance(exc, WebDriverException):
            raise FlipstarterUnavailableException(f'Unable to retrieve flipstarter {title} from {url} - {exc}')
        raise exc
    finally:
        if driver is not None:
            driver.quit()

    # load the image into memory so that we can make use of django storage api
    # to save it in the appropriate place
    return (TemporaryFile(screenshot_path), screenshot_name)
