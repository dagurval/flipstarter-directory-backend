from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page

from rest_framework import status
from rest_framework.decorators import throttle_classes
from rest_framework.generics import GenericAPIView, ListAPIView
from rest_framework.mixins import CreateModelMixin, ListModelMixin
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import GenericViewSet

from . import models
from . import serializers
from .throttling import (
    MinutelyAnonRateThrottle, HourlyAnonRateThrottle, DailyAnonRateThrottle
)


class FlipstarterCategoryView(ListAPIView):
    queryset = models.FlipstarterCategory.objects.all()
    serializer_class = serializers.FlipstarterCategorySerializer

    # cache for a 5 minutes
    @method_decorator(cache_page(300))
    def list(self, request, *args, **kwargs):
        return super().list(request, *args, **kwargs)


class FlipstarterFetchDataFromURL(GenericAPIView):
    """
    View that receives a url and return campaign data if any available
    """
    serializer_class = serializers.FlipstarterCampaignURLSerializer

    throttle_classes = [DailyAnonRateThrottle, HourlyAnonRateThrottle, MinutelyAnonRateThrottle]

    def post(self, request, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            serializer.fetch_data_from_url()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response('No url provided.', status=status.HTTP_400_BAD_REQUEST)


class FlipstarterViewSet(CreateModelMixin,
                         ListModelMixin,
                         GenericViewSet):

    serializer_class = serializers.FlipstarterSerializer

    def get_queryset(self):
        """
        Only return flipstarters that are approved
        """
        return models.Flipstarter.objects.prefetch_related(
            'categories',
            'announcements',
            'archives'
        ).filter(
            approved=True
        )

    def old(self):
        return not self.request.query_params.get('old', False) == False

    def get_serializer_class(self):
        if self.old() and self.action == 'list':
            return serializers.FlipstarterJSONFormatSerializer

        return super().get_serializer_class()

    # throttle create so we don't get spammed too much
    @throttle_classes([DailyAnonRateThrottle, HourlyAnonRateThrottle, MinutelyAnonRateThrottle])
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    @method_decorator(cache_page(300))
    def list(self, request, *args, **kwargs):
        return super().list(request, *args, **kwargs)
