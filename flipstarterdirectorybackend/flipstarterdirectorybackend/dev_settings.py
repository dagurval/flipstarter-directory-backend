from .base_settings import *

# change as you need for dev only options

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '@dbg7_7p2-d5%x1uk2oz3crcz_3o19=r25diky%@$yj^f74lwa'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = []

# Database
# https://docs.djangoproject.com/en/3.1/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': BASE_DIR / 'db.sqlite3',
    }
    # 'default': {
    #     'ENGINE': 'django.db.backends.postgresql',
    #     'NAME': 'devdb',
    #     'USER': 'devuser',
    #     'PASSWORD': 'devpassword',
    #     'HOST': '127.0.0.1',
    #     'PORT': '5432',
    # }
}

CORS_ALLOW_ALL_ORIGINS = True

MEDIA_ROOT = os.path.join(BASE_DIR, 'media/')
